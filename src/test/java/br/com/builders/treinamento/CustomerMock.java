package br.com.builders.treinamento;

import br.com.builders.treinamento.domain.Customer;

public final class CustomerMock {

	public static final String ID = "777fa88c-4511-445c-b33a-ddff58d76886";
	public static final String CRM_ID = "C645235";
	public static final String BASE_URL = "http://www.platformbuilders.com.br";
	public static final String NAME = "Platform Builders";
	public static final String LOGIN = "contato@platformbuilders.com.br";

	public static Customer get() {
		Customer customer = new Customer();
		customer.setId(CustomerMock.ID);
		customer.setCrmId(CustomerMock.CRM_ID);
		customer.setBaseUrl(CustomerMock.BASE_URL);
		customer.setName(CustomerMock.NAME);
		customer.setLogin(CustomerMock.LOGIN);
		return customer;
	}

}
