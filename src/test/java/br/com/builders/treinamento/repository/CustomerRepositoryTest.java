package br.com.builders.treinamento.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.builders.treinamento.domain.Customer;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerRepositoryTest {

	@Autowired
	private CustomerRepository repository;

	@Test
	public void createNewCustomer() {
		Customer c = getCustomer();
		try {
			assertNull(repository.findOne(c.getId()));

			repository.save(c);

			assertNotNull(repository.findOne(c.getId()));
		} finally {
			repository.delete(c);
		}
	}

	@Test
	public void deleteNewCustomer() {
		long berofe = repository.findAll().size();
		
		Customer c = getCustomer();
		repository.save(c);

		long after = repository.findAll().size();
		assertEquals(berofe + 1, after);

		repository.delete(getCustomer());
		long afterFinal = repository.findAll().size();
		assertEquals(berofe, afterFinal);
	}

	private Customer getCustomer() {
		Customer customer = new Customer();
		customer.setId("978fa88c-4511-445c-b33a-ddff58d76886");
		customer.setCrmId("C645235");
		customer.setBaseUrl("http://www.platformbuilders.com.br");
		customer.setName("Platform Builders");
		customer.setLogin("contato@platformbuilders.com.br");
		return customer;
	}

}
