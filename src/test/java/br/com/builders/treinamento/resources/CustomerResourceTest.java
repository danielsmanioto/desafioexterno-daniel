package br.com.builders.treinamento.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.builders.treinamento.CustomerMock;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.utils.JsonUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerResourceTest {

	private static final String CONTENT_TYPE = "application/json;charset=UTF8";
	
	private MockMvc mvc;

	@Autowired
	private WebApplicationContext context;

	@LocalServerPort
	private int port;

	@Autowired
	private CustomerResource controller;
	
	@Autowired
	private CustomerRepository repository;
	
	@Before
	public void setup() throws UnknownHostException, IOException {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void validInit() {
		assertThat(controller).isNotNull();
	}

	@Test
	public void seachAllCustomers() {
		Customer customer = CustomerMock.get();
		
		try {
			long countBefore = repository.findAll().size();
			
			repository.save(customer);
			
			long countAfter = repository.findAll().size();

			mvc.perform(get("/customers")).andExpect(status().isOk()).andExpect(content().contentType(CONTENT_TYPE))
				.andExpect(jsonPath("$[0].name", containsString(CustomerMock.NAME)));
			
			assertEquals(countBefore+1, countAfter);
		} catch (Exception e) {
			assertTrue(false);
		} finally {
			repository.delete(customer);
		}
	}

	@Test
	public void createNewCustomer() {
		Customer c = CustomerMock.get();
		String json = JsonUtil.fromObject(c);
		
		try {
			this.mvc.perform(post("/customers").contentType(MediaType.APPLICATION_JSON).content(json))
					.andExpect(status().isCreated());

			Customer customerCreated = repository.findOne(c.getId());

			assertEquals(c, customerCreated);
		} catch (Exception e) {
			assertTrue(false);
		} finally {
			repository.delete(c);
		}
	}

	@Test
	public void removeCustomer() {
		try {
			long countBefore = repository.findAll().size();
			
			Customer customer = CustomerMock.get();
			repository.save(customer);
			String json = JsonUtil.fromObject(customer);
			
			long countAfter = repository.findAll().size();

			assertEquals(countBefore+1, countAfter);
			
			mvc.perform(delete("/customers/" + CustomerMock.ID).contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());
			
			long countAfterFinal = repository.findAll().size();
			
			assertEquals(countBefore, countAfterFinal);
		} catch (Exception e) {
			assertTrue(false);
		} 
	}

	@Test
	public void updateCustomer() {
		Customer customer = CustomerMock.get();
		
		try {
			long countBefore = repository.findAll().size();
			
			repository.save(customer);
			
			long countAfter = repository.findAll().size();
			
			assertEquals(countBefore+1, countAfter);
			
			assertEquals(customer, repository.findOne(customer.getId()));
			
			Customer customerUpdate = CustomerMock.get();
			customerUpdate.setName("alterado");
			String json = JsonUtil.fromObject(customerUpdate);
			mvc.perform(put("/customers/" + CustomerMock.ID).contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(status().isOk());
			
			Customer c = repository.findOne(customerUpdate.getId());
			assertEquals("alterado", c.getName());

		} catch (Exception e) {
			assertTrue(false);
		} finally {
			repository.delete(customer);
		}
	}

	@Test
	public void findOneCustomer() {
		Customer customer = CustomerMock.get();
		
		try {
			long countBefore = repository.findAll().size();
			
			repository.save(customer);
			
			long countAfter = repository.findAll().size();
			assertEquals(countBefore+1, countAfter);
			
			mvc.perform(get("/customers/" + CustomerMock.ID)).andExpect(status().isOk()).andExpect(content().contentType(CONTENT_TYPE))
			.andExpect(jsonPath("$.name", equalTo(CustomerMock.NAME)));

		} catch (Exception e) {
			assertTrue(false);
		} finally {
			repository.delete(customer);
		}
		
	}

}
