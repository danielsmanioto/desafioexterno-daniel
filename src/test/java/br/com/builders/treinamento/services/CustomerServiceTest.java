package br.com.builders.treinamento.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.builders.treinamento.CustomerMock;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.APIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void insertGetAllCustomer() throws APIException {
		Customer c = CustomerMock.get();
		try {
			Customer newCustomer = customerService.save(c);
			assertEquals(CustomerMock.ID, newCustomer.getId());
			assertEquals(CustomerMock.CRM_ID, newCustomer.getCrmId());
			assertEquals(CustomerMock.BASE_URL, newCustomer.getBaseUrl());
			assertEquals(CustomerMock.NAME, newCustomer.getName());
			assertEquals(CustomerMock.LOGIN, newCustomer.getLogin());
		
			newCustomer = customerService.getCustomerById(CustomerMock.ID);
			assertEquals(CustomerMock.ID, newCustomer.getId());
			assertEquals(CustomerMock.CRM_ID, newCustomer.getCrmId());
			assertEquals(CustomerMock.BASE_URL, newCustomer.getBaseUrl());
			assertEquals(CustomerMock.NAME, newCustomer.getName());
			assertEquals(CustomerMock.LOGIN, newCustomer.getLogin());
		} finally {
			customerRepository.delete(c);
		}
	}

}
