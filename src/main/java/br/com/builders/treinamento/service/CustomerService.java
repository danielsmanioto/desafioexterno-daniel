package br.com.builders.treinamento.service;

import java.util.List;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.APIException;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.InternalServerErrorException;
import br.com.builders.treinamento.exception.NotFoundException;

public interface CustomerService {

	public List<Customer> getCustomers() throws APIException; 

	public Customer getCustomerById(final String id) throws APIException;

	public Customer save(final Customer customer) throws BadRequestAPIException, InternalServerErrorException;

	public Customer update(Customer customer) throws BadRequestAPIException, NotFoundException;

	public void remove(Customer customer);
	
	public void removeAll();
	
}
