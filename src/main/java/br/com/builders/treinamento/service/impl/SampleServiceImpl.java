/*
* Copyright 2018 Builders
*************************************************************
*Nome     : BuildersService.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import br.com.builders.treinamento.service.SampleService;

@Component
@Lazy
public class SampleServiceImpl implements SampleService {

	@Value("${host:'http://localhost:8080/WebService?wsdl'}")
	protected String host;

	@Value("${login:'teste'}")
	protected String login;

	@Value("${senha:'teste'}")
	protected String senha;
	
}
