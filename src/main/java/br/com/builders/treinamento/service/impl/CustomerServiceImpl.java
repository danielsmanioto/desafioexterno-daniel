package br.com.builders.treinamento.service.impl;

import java.util.List;

import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.InternalServerErrorException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	private final static Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private CustomerRepository repository;

	@Override
	public List<Customer> getCustomers() {
		return repository.findAll();
	}

	@Override
	public Customer getCustomerById(final String id) throws NotFoundException {
		Customer c = repository.findOne(id);
		if (c != null) {
			return c;
		} else {
			final String error = "Not found to find customer by id [" + id + "]";
			log.info(error);
			throw new NotFoundException(error);
		}

	}

	@Override
	public Customer save(final Customer customer) throws BadRequestAPIException, InternalServerErrorException {
		try {
			repository.save(customer);
		} catch (BadRequestException e ) {
			log.info("Bad Request");
			throw new BadRequestAPIException("400");
		} catch (Exception e ) {
			log.info("Generic error to sabe customer");
			throw new InternalServerErrorException(e);
		}
		
		return customer;
	}
	
	@Override
	public Customer update(final Customer customer) throws BadRequestAPIException, NotFoundException {
		try {
			repository.save(customer);
		} catch (BadRequestException e ) {
			log.info("Bad Request");
			throw new BadRequestAPIException("400");
		} catch (Exception e ) {
			log.info("Generic error to sabe customer");
			throw new NotFoundException(e.toString());
		}
		
		return customer;
		
	}
	
	@Override
	public void remove(Customer customer) {
		repository.delete(customer);
	}

	@Override
	public void removeAll() {
		List<Customer> customers = repository.findAll();
		customers.forEach(c -> {
			repository.delete(c);
		});
	}

}
