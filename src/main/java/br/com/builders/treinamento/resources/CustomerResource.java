package br.com.builders.treinamento.resources;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.InternalServerErrorException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.service.impl.CustomerServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/customers")
@Api(value = "API Customer")
public class CustomerResource {

	@Autowired
	private CustomerServiceImpl service;

	@ApiOperation(value = "List all customers / containers (with essential data, enough for displaying a table)", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get") })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<Customer>> getCustomers() {
		
		final List<Customer> customers = service.getCustomers();
		
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}

	@ApiOperation(value = "Search results matching criteria", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Get") })
	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Customer> getCustomersById(@PathVariable(required = true) final String customerId) throws NotFoundException {

		Customer customer = service.getCustomerById(customerId);

		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@ApiOperation(value = "Insert a new customer", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Post") })
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<Customer> insert(@RequestBody Customer customer) throws BadRequestAPIException, InternalServerErrorException {
		
		service.save(customer);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update patch customer/{customerId}", httpMethod = "PATCH")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "PATCH") })
	@RequestMapping(value = "/{customerId}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Customer> updatePatch(@RequestBody Customer customer) throws BadRequestAPIException, NotFoundException {
		
		service.update(customer);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@ApiOperation(value = "Update customer", httpMethod = "PUT")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Put") })
	@RequestMapping(value = "/{customerId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Customer> update(@RequestBody Customer customer) throws BadRequestAPIException, NotFoundException {
		
		service.update(customer);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete customer", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Delete") })
	@RequestMapping(value = "/{customerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Customer> remove(@RequestBody Customer customer) {
		
		service.remove(customer);
		
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete all customers", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Delete") })
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<Customer>> deleteAll() {
		service.removeAll();

		final List<Customer> customers = service.getCustomers();
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}

}
