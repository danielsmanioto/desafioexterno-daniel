/*
* Copyright 2018 Builders
*************************************************************
*Nome     : Cartao.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.domain;

import org.hibernate.validator.constraints.NotBlank;

import br.com.builders.treinamento.exception.ErrorCodes;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Sample {

	@NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
	private String idSample;

	public Sample() {
		// Constructor default
	}

	public String getIdSample() {
		return idSample;
	}

	public void setIdSample(String idSample) {
		this.idSample = idSample;
	}
	
	@Override
	public String toString() {
		return "Sample [idSample=" + idSample + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idSample == null) ? 0 : idSample.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sample other = (Sample) obj;
		if (idSample == null) {
			if (other.idSample != null)
				return false;
		} else if (!idSample.equals(other.idSample))
			return false;
		return true;
	}
	

}
