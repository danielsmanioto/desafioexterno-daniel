package br.com.builders.treinamento.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@ApiModel(value = "CustomerRequest")
public class CustomerResponse {

	@ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world.")
	private String id;

	@ApiModelProperty(example = "C645235", value = "Customer ID in the CRM.")
	private String crmId;

	@ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container.")
	private String baseUrl;

	@ApiModelProperty(example = "Platform Builders", value = "Customer name")
	private String name;

	@ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login")
	private String login;

	public CustomerResponse() {
		// Constructor default
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "CustomerRequest [id=" + id + ", crmId=" + crmId + ", baseUrl=" + baseUrl + ", name=" + name + ", login="
				+ login + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
		result = prime * result + ((crmId == null) ? 0 : crmId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerResponse other = (CustomerResponse) obj;
		if (baseUrl == null) {
			if (other.baseUrl != null)
				return false;
		} else if (!baseUrl.equals(other.baseUrl))
			return false;
		if (crmId == null) {
			if (other.crmId != null)
				return false;
		} else if (!crmId.equals(other.crmId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
