package br.com.builders.treinamento.dto.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

import br.com.builders.treinamento.exception.ErrorCodes;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@ApiModel(value = "CustomerRequest")
public class CustomerRequest {

    @Id
    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    private String id;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    private String crmId;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    private String baseUrl;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
	private String name;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
    private String login;

	public CustomerRequest() {
		// Constructor default
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "CustomerRequest [id=" + id + ", crmId=" + crmId + ", baseUrl=" + baseUrl + ", name=" + name + ", login="
				+ login + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
		result = prime * result + ((crmId == null) ? 0 : crmId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerRequest other = (CustomerRequest) obj;
		if (baseUrl == null) {
			if (other.baseUrl != null)
				return false;
		} else if (!baseUrl.equals(other.baseUrl))
			return false;
		if (crmId == null) {
			if (other.crmId != null)
				return false;
		} else if (!crmId.equals(other.crmId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
